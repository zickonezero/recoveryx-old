var React         = require('react'),
    ReactDOM      = require('react-dom'),
    Surfing = require('./components/Surfing.jsx');

ReactDOM.render(
  <Surfing/>,
  document.getElementById("react-container")
);
