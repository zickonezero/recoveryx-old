var React         = require('react'),
    ReactDOM      = require('react-dom'),
    Main = require('./components/Main.jsx');

ReactDOM.render(
  <Main dataFeed={workData}/>,
  document.getElementById("react-container")
);
