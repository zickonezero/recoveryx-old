var React         = require('react'),
    ReactDOM      = require('react-dom'),
    Contact       = require('./components/Contact.jsx');

ReactDOM.render(
  <Contact/>,
  document.getElementById("react-container")
);
